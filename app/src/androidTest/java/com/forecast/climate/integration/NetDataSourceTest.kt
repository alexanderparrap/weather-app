package com.forecast.climate.integration


import com.forecast.climate.data.source.net.NetDataSource
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before

import org.junit.Rule
import org.junit.Test
import javax.inject.Inject

@HiltAndroidTest
class NetDataSourceTest {

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Inject
    lateinit var netDataSource: NetDataSource

    private val lat = "33.86".toDouble()
    private val lon = "151.20".toDouble()
    private val exclude = "hourly,minutely,alerts,current"
    private val appId = "4493188b412e5452e0fbdda35fb16bcf"
    private val units = "metric"

    @Before
    fun setup() {
        hiltRule.inject()
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun getWeatherForecastTest()= runBlocking {
        val response = netDataSource.forecast(lat, lon, exclude, appId, units)
        assertEquals(response.code(), 200)
        assertEquals(lat, response.body()?.lat)
    }

}