package com.forecast.climate.datasource

import com.forecast.climate.data.factory.WeatherDataFactory
import com.forecast.climate.data.source.local.LocalDataSource
import com.forecast.climate.data.source.net.NetDataSource
import com.forecast.climate.rules.CoroutineTestRule
import com.forecast.climate.util.Samples
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.Response

@RunWith(MockitoJUnitRunner::class)
class NetDataSourceTest {
    @ExperimentalCoroutinesApi
    @get:Rule
    val coroutineTestRule = CoroutineTestRule()
    @Mock
    private lateinit var netDataSource: NetDataSource
    @Mock
    private lateinit var localDataSource: LocalDataSource

    private lateinit var factory: WeatherDataFactory
    private val lat = "33.86".toDouble()
    private val lon = "151.20".toDouble()
    private val exclude = "hourly,minutely,alerts,current"
    private val appId = "4493188b412e5452e0fbdda35fb16bcf"
    private val units = "metric"

    @Before
    fun setup(){
        MockitoAnnotations.openMocks(this)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun `get weather forecast test with successful response`() = runBlockingTest {
        //Given
        factory = WeatherDataFactory(netDataSource, localDataSource)

        Mockito.`when`(netDataSource.forecast(lat, lon, exclude, appId, units))
            .thenReturn(Response.success(Samples.forecast))

        //When
        val response = factory.getWeatherForecast(lat, lon, exclude, appId, units)

        //Then
        Mockito.verify(netDataSource, Mockito.times(1)).forecast(lat, lon, exclude, appId, units)
        assertEquals(response.body(), Samples.forecast)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun `get weather forecast test with error response`() = runBlockingTest {
        //Given
        factory = WeatherDataFactory(netDataSource, localDataSource)

        Mockito.`when`(netDataSource.forecast(lat, lon, exclude, appId, units))
            .thenReturn(Response.error(401,Samples.responseBody.toResponseBody("application/json; charset=utf-8".toMediaType())))

        //When
        val response = factory.getWeatherForecast(lat, lon, exclude, appId, units)

        //Then
        Mockito.verify(netDataSource, Mockito.times(1)).forecast(lat, lon, exclude, appId, units)
        assertEquals(response.code(), 401)
    }
}