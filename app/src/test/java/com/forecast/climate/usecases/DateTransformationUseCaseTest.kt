package com.forecast.climate.usecases

import com.forecast.climate.domain.usecases.DateTransformationUseCase
import com.forecast.climate.domain.usecases.WeatherForecastUseCase
import com.forecast.climate.presentation.forecast.ForecastViewModel
import com.forecast.climate.util.Samples
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class DateTransformationUseCaseTest {

    @Mock
    private lateinit var weatherForecastUseCase: WeatherForecastUseCase
    @Mock
    private lateinit var dateTransformationUseCase: DateTransformationUseCase

    private lateinit var forecastViewModel: ForecastViewModel

    @Before
    fun setup(){
        MockitoAnnotations.openMocks(this)
        forecastViewModel = ForecastViewModel(weatherForecastUseCase, dateTransformationUseCase)
        forecastViewModel.setLocation(Samples.location)
    }

    @Test
    fun `transform time stamp to string date test`() {

        val timestamp = 1649034000
        val response = "April 04, 2022"

        //Given
        Mockito.`when`(dateTransformationUseCase.convertTimeStampDateToDateFormat(timestamp.toLong()))
            .thenReturn(response)

        //When
        val result = forecastViewModel.convertTimeStampToDateFormat(timestamp.toLong())

        //Then
        Mockito.verify(dateTransformationUseCase, Mockito.times(1))
            .convertTimeStampDateToDateFormat(timestamp.toLong())
        Assert.assertEquals(result, response)
    }

    @Test
    fun `retrieve the day of the week test`() {
        val timestamp = 1649034000L
        val response = "Monday"

        //Given
        Mockito.`when`(dateTransformationUseCase.getDayOfWeekFromTimeStamp(timestamp))
            .thenReturn(response)

        //When
        val result = forecastViewModel.getDayOfWeek(timestamp)

        //Then
        Mockito.verify(dateTransformationUseCase, Mockito.times(1))
            .getDayOfWeekFromTimeStamp(timestamp)
        Assert.assertEquals(result, response)
    }
}