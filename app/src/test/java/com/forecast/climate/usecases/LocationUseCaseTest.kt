package com.forecast.climate.usecases

import com.forecast.climate.domain.usecases.LocationUseCase
import com.forecast.climate.presentation.recent.RecentPlacesViewModel
import com.forecast.climate.presentation.search.SearchPlaceViewModel
import com.forecast.climate.util.Samples
import com.forecast.climate.util.PlaceMock
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.mockito.internal.verification.Times
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class LocationUseCaseTest {
    @Mock
    private lateinit var locationUseCase: LocationUseCase

    private var place =  PlaceMock()

    private lateinit var searchPlaceViewModel: SearchPlaceViewModel
    private lateinit var recentPlacesViewModel: RecentPlacesViewModel

    @Before
    fun setup(){
        MockitoAnnotations.openMocks(this)
        searchPlaceViewModel = SearchPlaceViewModel(locationUseCase)
        recentPlacesViewModel = RecentPlacesViewModel(locationUseCase)
    }

    @Test
    fun `save selected location` (){

        //Given
        doNothing().`when`(locationUseCase).saveRecentLocation(Samples.location)

        //When
        searchPlaceViewModel.setLatAndLon(place)
        searchPlaceViewModel.saveSelectedLocation()

        //Then
        verify(locationUseCase, Times(1)).saveRecentLocation(Samples.location)
    }

    @Test
    fun `retrieve recent location` (){

        //Given
        `when`(locationUseCase.retrieveStoredLocation()).thenAnswer { mutableListOf(Samples.location) }

        //When
        val recentPlaces = recentPlacesViewModel.retrieveSavedLocation()

        //Then
        verify(locationUseCase, Times(1)).retrieveStoredLocation()
        Assert.assertEquals(recentPlaces.first().name, "Canberra")
    }

}