package com.forecast.climate.usecases

import com.forecast.climate.domain.models.net.NetworkResult
import com.forecast.climate.domain.usecases.DateTransformationUseCase
import com.forecast.climate.domain.usecases.WeatherForecastUseCase
import com.forecast.climate.presentation.forecast.ForecastViewModel
import com.forecast.climate.rules.CoroutineTestRule
import com.forecast.climate.util.Samples
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class WeatherForecastUseCaseTest {

    @ExperimentalCoroutinesApi
    @get:Rule
    val coroutineTestRule = CoroutineTestRule()
    @Mock
    private lateinit var weatherForecastUseCase: WeatherForecastUseCase
    @Mock
    private lateinit var dateTransformationUseCase: DateTransformationUseCase
    @Mock
    private lateinit var callback:() -> Unit

    private lateinit var forecastViewModel: ForecastViewModel

    @Before
    fun setup(){
        MockitoAnnotations.openMocks(this)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun `retrieve weather forecast test with successful response`() = runBlockingTest {

        val lat = "33.86".toDouble()
        val lon = "151.20".toDouble()

        //Given
        forecastViewModel = ForecastViewModel(weatherForecastUseCase, dateTransformationUseCase)
        forecastViewModel.setLocation(Samples.location)

        `when`(weatherForecastUseCase.retrieveWeatherForecast(lat,lon))
            .thenAnswer { (flow { emit(NetworkResult.Success(Samples.forecast)) }) }

        //When
        forecastViewModel.retrieveWeather({},callback)

        //Then
        verify(weatherForecastUseCase, times(1)).retrieveWeatherForecast(lat, lon)
        verify(callback, atLeastOnce()).invoke()
    }
}