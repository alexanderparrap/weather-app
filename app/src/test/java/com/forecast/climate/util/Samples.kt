package com.forecast.climate.util

import com.forecast.climate.domain.models.*
import kotlin.math.min

object Samples {

    val forecast = Forecast(
        lat = 33.86,
        lon = 151.20,
        timezone = "Etc/GMT-10",
        timezone_offset = 36000,
        daily = mutableListOf(DailyForecast(
            dt = 1649034000,
            sunrise = 1649014757,
            sunset = 1649060223,
            moonrise = 1649020440,
            moonset =  1649070780,
            moon_phase = 0.09,
            temp = Temperature(
                day = 13.55,
                min = 13.45,
                max = 14.8,
                night = 13.8,
                eve = 13.56,
                morn = 14.18
            ),
            feels_like = FeelsLike(
                day = 13.21,
                night = 14.79,
                eve = 13.24,
                morn = 13.69
            ),
            pressure = 1028,
            humidity = 86,
            dew_point = 11.26,
            wind_speed = 18.56,
            wind_deg = 91,
            wind_gust = 22.49,
            weather = listOf(Weather(
                id = 582,
                main = "Rain",
                description = "heavy intensity rain",
                icon = "10d"
            )),
            clouds = 100,
            pop = 1.0,
            rain = 42.04,
            uvi = 1.29
        ))
    )

    val location = Location(
        name = "Canberra",
        lat = 33.86,
        lon = 151.20
    )

    const val responseBody = "{\n" +
            "    \"cod\": 401,\n" +
            "    \"message\": \"Invalid API key. Please see http://openweathermap.org/faq#error401 for more info.\"\n" +
            "}"
}