package com.forecast.climate.util

import android.net.Uri
import android.os.Parcel
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.libraries.places.api.model.*

class PlaceMock : Place() {
    override fun describeContents(): Int {
        TODO("Not yet implemented")
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("Not yet implemented")
    }

    override fun getWebsiteUri(): Uri? {
        TODO("Not yet implemented")
    }

    override fun getLatLng(): LatLng? {
        return LatLng(33.86, 151.20)
    }

    override fun getViewport(): LatLngBounds? {
        TODO("Not yet implemented")
    }

    override fun getAddressComponents(): AddressComponents? {
        TODO("Not yet implemented")
    }

    override fun getOpeningHours(): OpeningHours? {
        TODO("Not yet implemented")
    }

    override fun getBusinessStatus(): BusinessStatus? {
        TODO("Not yet implemented")
    }

    override fun getPlusCode(): PlusCode? {
        TODO("Not yet implemented")
    }

    override fun getRating(): Double? {
        TODO("Not yet implemented")
    }

    override fun getIconBackgroundColor(): Int? {
        TODO("Not yet implemented")
    }

    override fun getPriceLevel(): Int? {
        TODO("Not yet implemented")
    }

    override fun getUserRatingsTotal(): Int? {
        TODO("Not yet implemented")
    }

    override fun getUtcOffsetMinutes(): Int? {
        TODO("Not yet implemented")
    }

    override fun getAddress(): String? {
        TODO("Not yet implemented")
    }

    override fun getIconUrl(): String? {
        TODO("Not yet implemented")
    }

    override fun getId(): String? {
        TODO("Not yet implemented")
    }

    override fun getName(): String? {
        return "Canberra"
    }

    override fun getPhoneNumber(): String? {
        TODO("Not yet implemented")
    }

    override fun getAttributions(): MutableList<String>? {
        TODO("Not yet implemented")
    }

    override fun getPhotoMetadatas(): MutableList<PhotoMetadata>? {
        TODO("Not yet implemented")
    }

    override fun getTypes(): MutableList<Type>? {
        TODO("Not yet implemented")
    }
}