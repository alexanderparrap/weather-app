package com.forecast.climate.presentation.recent

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import com.forecast.climate.R
import com.forecast.climate.databinding.RecentPlacesFragmentBinding
import com.forecast.climate.presentation.search.LOCATION
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RecentPlacesFragment : Fragment() {

    private val viewModel by viewModels<RecentPlacesViewModel>()
    private var _binding: RecentPlacesFragmentBinding? = null
    private val binding get() =  _binding!!
    private lateinit var adapter : RecentLocationListAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = RecentPlacesFragmentBinding.inflate(inflater, container, false)
        binding.deleteButton.setOnClickListener {
            viewModel.deleteLocations()
        }
        adapter = RecentLocationListAdapter(viewModel){
            val bundle = bundleOf(LOCATION to it)
            Navigation.findNavController(binding.root).navigate(R.id.action_navigation_recent_to_navigation_forecast, bundle)
        }
        binding.returnToSearch.setOnClickListener {
            Navigation.findNavController(binding.root).navigate(R.id.action_navigation_recent_to_navigation_search)
        }

        binding.deleteButton.setOnClickListener {
            viewModel.deleteLocations()
            adapter.notifyDataSetChanged()
        }
        binding.locationList.adapter = adapter

        return binding.root
    }

}