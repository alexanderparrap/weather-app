package com.forecast.climate.presentation.forecast

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.forecast.climate.R
import com.forecast.climate.databinding.ForecastFragmentBinding
import com.forecast.climate.presentation.search.LOCATION
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ForecastFragment : Fragment() {

    private val viewModel by viewModels<ForecastViewModel>()
    private var _binding: ForecastFragmentBinding? = null
    private val binding get() = _binding!!
    private lateinit var adapter: ForecastListAdapter

    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = ForecastFragmentBinding.inflate(inflater, container, false)
        viewModel.setLocation(requireArguments().getParcelable(LOCATION)!!)

        binding.forecastTitle.text = "${resources.getString(R.string.the_forecast_in)} ${viewModel.getLocation().name} ${resources.getString(R.string.over_the_next)}"

        viewModel.retrieveWeather({
            binding.progressBar.visibility = it
        }) {
            adapter = ForecastListAdapter(viewModel)
            val manager = LinearLayoutManager(requireActivity(), LinearLayoutManager.HORIZONTAL, false)
            binding.forecastList.layoutManager = manager
            binding.forecastList.adapter = adapter
        }

        binding.changeAddress.setOnClickListener { view ->
            Navigation.findNavController(view).navigate(R.id.action_navigation_forecast_to_navigation_search)
        }
        return binding.root
    }
}