package com.forecast.climate.presentation.forecast

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.forecast.climate.R
import com.forecast.climate.data.util.Constants
import com.forecast.climate.databinding.ForecastItemBinding
import com.forecast.climate.domain.models.DailyForecast
import kotlin.math.roundToInt

class ForecastListAdapter(
    private var viewModel: ForecastViewModel
    ): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = ForecastItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ForecastViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder){
            is ForecastViewHolder -> {
                val dailyForecast = viewModel.getForecast().daily[position]
                holder.bind(dailyForecast)
            }
        }
    }

    override fun getItemCount(): Int = viewModel.getForecast().daily.size

    inner class ForecastViewHolder(private val binding: ForecastItemBinding): RecyclerView.ViewHolder(binding.root){
        @SuppressLint("SetTextI18n")
        fun bind(forecast: DailyForecast?){
            forecast?.let {
                binding.dayOfWeek.text = viewModel.getDayOfWeek(forecast.dt)
                binding.date.text = viewModel.convertTimeStampToDateFormat(forecast.dt)
                Glide.with(binding.root.context)
                    .load("${Constants.ICON_URL}${forecast.weather.first().icon}@2x.png")
                    .placeholder(R.drawable.ic_baseline_cloud_24)
                    .centerCrop()
                    .into(binding.weatherIcon)
                binding.temperature.text = "${forecast.temp.day.roundToInt()}°C"
                binding.weatherDescription.text = forecast.weather.first().description
                binding.maxAndMinDecription.text = "${binding.root.context.resources.getString(R.string.the_high_will_be)} ${forecast.temp.max.roundToInt()}°C, ${binding.root.context.resources.getString(R.string.the_low_will_be)} ${forecast.temp.min.roundToInt()}°C."
            }
        }
    }

}