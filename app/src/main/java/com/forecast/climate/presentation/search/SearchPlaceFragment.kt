package com.forecast.climate.presentation.search

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import com.forecast.climate.BuildConfig
import com.forecast.climate.R
import com.forecast.climate.data.util.Constants
import com.forecast.climate.databinding.SearchPlaceFragmentBinding
import com.google.android.gms.common.api.Status
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import dagger.hilt.android.AndroidEntryPoint

const val TAG = "Search"
const val LOCATION = "location"

@AndroidEntryPoint
class SearchPlaceFragment : Fragment() {

    private val viewModel by viewModels<SearchPlaceViewModel>()
    private var _binding: SearchPlaceFragmentBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (!Places.isInitialized()){
            Places.initialize(requireContext(),BuildConfig.apiKey)
        }
        val placesClient = Places.createClient(requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = SearchPlaceFragmentBinding.inflate(inflater, container, false)
        val autocompleteFragment = childFragmentManager.findFragmentById(R.id.autocomplete_fragment)
                    as AutocompleteSupportFragment

        autocompleteFragment.setPlaceFields(listOf(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG))
        autocompleteFragment.setCountry(Constants.COUNTRY_INITIALS)

        autocompleteFragment.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(place: Place) {
                Log.i(TAG, "Place: ${place.name}, ${place.id}, ${place.latLng} ")
                viewModel.setLatAndLon(place)
            }

            override fun onError(status: Status) {
                Log.i(TAG, "An error occurred: $status")
            }
        })

        binding.searchButton.setOnClickListener { view ->
            viewModel.proceedIfLocationIsNotEmpty {
                viewModel.saveSelectedLocation()
                val bundle = bundleOf(LOCATION to viewModel.getLocation())
                Navigation.findNavController(view).navigate(R.id.action_navigation_search_to_navigation_forecast, bundle)
            }
        }

        binding.recentPlacesButton.setOnClickListener { view ->
            Navigation.findNavController(view).navigate(R.id.action_navigation_search_to_navigation_recent)
        }

        return binding.root
    }

}