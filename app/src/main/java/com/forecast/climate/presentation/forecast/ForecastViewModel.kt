package com.forecast.climate.presentation.forecast

import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.forecast.climate.domain.models.Forecast
import com.forecast.climate.domain.models.Location
import com.forecast.climate.domain.models.net.NetworkResult
import com.forecast.climate.domain.usecases.DateTransformationUseCase
import com.forecast.climate.domain.usecases.WeatherForecastUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

const val TAG = "Forecast"

@HiltViewModel
class ForecastViewModel @Inject constructor(
    private val weatherForecastUseCase: WeatherForecastUseCase,
    private val dateTransformationUseCase: DateTransformationUseCase
) : ViewModel() {
    private lateinit var location:Location
    private lateinit var forecast: Forecast

    fun setLocation(location: Location){
        this.location = location
    }
    fun getLocation():Location = location

    fun getForecast():Forecast = forecast

    fun retrieveWeather(loadingCallBack:(Int) -> Unit, callback:() -> Unit){
        viewModelScope.launch {
            weatherForecastUseCase.retrieveWeatherForecast(location.lat,location.lon)
                .collect {
                    when(it){
                        is NetworkResult.Success -> {
                            forecast = it.data!!
                            callback.invoke()
                        }
                        is NetworkResult.Loading -> {
                            if(it.isLoading){
                                loadingCallBack.invoke(View.VISIBLE)
                            } else {
                                loadingCallBack.invoke(View.GONE)
                            }

                        }
                        is NetworkResult.Error -> { Log.i(TAG, "Error: ${it.message}")}
                    }
                }
        }
    }

    fun getDayOfWeek(timeStamp:Long): String {
        return dateTransformationUseCase.getDayOfWeekFromTimeStamp(timeStamp)
    }
    fun convertTimeStampToDateFormat(timeStamp:Long): String {
        return dateTransformationUseCase.convertTimeStampDateToDateFormat(timeStamp)
    }
}