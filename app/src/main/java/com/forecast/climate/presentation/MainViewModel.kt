package com.forecast.climate.presentation

import androidx.lifecycle.ViewModel
import com.forecast.climate.domain.models.Location
import com.forecast.climate.domain.usecases.LocationUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val locationUseCase: LocationUseCase
    ): ViewModel() {
    private val _hasStoredPlaces get()  = locationUseCase.retrieveStoredLocation().isNotEmpty()
    private val _retrieveLastLocation get() = locationUseCase.retrieveStoredLocation().last()

    fun handleStartNavigation(searchCallBack:()->Unit, forecastCallBack:(Location)->Unit){
        if(_hasStoredPlaces){
            forecastCallBack.invoke(_retrieveLastLocation)
        }else {
            searchCallBack.invoke()
        }
    }

}