package com.forecast.climate.presentation

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import com.forecast.climate.R
import com.forecast.climate.databinding.ActivityMainBinding
import com.forecast.climate.presentation.search.LOCATION
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val viewModel by viewModels<MainViewModel>()
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val navController = findNavController(R.id.nav_host_fragment)

        viewModel.handleStartNavigation({
            navController.navigate(R.id.navigation_search)
        }){
            val bundle = bundleOf(LOCATION to it)
            navController.navigate(R.id.navigation_forecast, bundle)
        }
    }
}
