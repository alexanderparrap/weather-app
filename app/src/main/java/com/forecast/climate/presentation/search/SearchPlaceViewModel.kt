package com.forecast.climate.presentation.search

import androidx.lifecycle.ViewModel
import com.forecast.climate.domain.models.Location
import com.forecast.climate.domain.usecases.LocationUseCase
import com.google.android.libraries.places.api.model.Place
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SearchPlaceViewModel @Inject constructor(
    private val locationUseCase: LocationUseCase
    ) : ViewModel() {
        private lateinit var location: Location

    fun setLatAndLon(place: Place){
        location = Location(
            place.name,
            place.latLng.latitude,
            place.latLng.longitude)

    }

    fun getLocation() = location

    fun saveSelectedLocation(){
        locationUseCase.saveRecentLocation(location)
    }

    fun proceedIfLocationIsNotEmpty(callback:() -> Unit) {
        if(::location.isInitialized){
            callback.invoke()
        }
    }

}