package com.forecast.climate.presentation.recent


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.forecast.climate.databinding.RecentPlacesItemBinding
import com.forecast.climate.domain.models.Location

class RecentLocationListAdapter(private val viewModel: RecentPlacesViewModel,
                                private val navigationCallback: (Location) -> Unit): RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = RecentPlacesItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return RecentLocationViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is RecentLocationViewHolder -> {
                holder.bind(viewModel.retrieveSavedLocation()[position])
            }
        }
    }

    override fun getItemCount(): Int = viewModel.retrieveSavedLocation().size

    inner class RecentLocationViewHolder(private val binding: RecentPlacesItemBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(location: Location){
            binding.locationName.text = location.name
            binding.latitude.text = location.lat.toString()
            binding.longitude.text = location.lon.toString()
            binding.checkBox.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked){
                    viewModel.selectedLocationToBeRemoved(location)
                } else {
                    viewModel.unSelectedLocationToBeRemoved(location)
                }
            }
            binding.root.setOnClickListener {
                navigationCallback.invoke(location)
            }
        }
    }
}