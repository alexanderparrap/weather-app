package com.forecast.climate.data.factory

import com.forecast.climate.data.source.local.LocalDataSource
import com.forecast.climate.data.source.net.NetDataSource
import com.forecast.climate.domain.models.Forecast
import com.forecast.climate.domain.models.Location
import com.forecast.climate.domain.models.net.NetworkResult
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class WeatherDataFactory @Inject constructor(
    private val netDataSource: NetDataSource,
    private val localDataSource: LocalDataSource
    ) {
    suspend fun getWeatherForecast(lat: Double, lon:Double, exclude: String, appId: String, units: String) = netDataSource.forecast(lat, lon, exclude, appId, units)
    fun saveListOfLocations(location: Location) = localDataSource.addToLocationList(location)
    fun getListOfLocations(): List<Location> = localDataSource.getLastLocationList()
    fun deleteLocations(deleteLocationList: List<Location>) = localDataSource.deleteLocations(deleteLocationList)
}