package com.forecast.climate.data

import com.forecast.climate.data.factory.WeatherDataFactory
import com.forecast.climate.domain.models.Forecast
import com.forecast.climate.domain.models.Location
import com.forecast.climate.domain.repository.WeatherRepository
import retrofit2.Response
import javax.inject.Inject

class WeatherDataRepository @Inject constructor(private val weatherDataFactory: WeatherDataFactory) : WeatherRepository {
    override suspend fun getForecast(lat: Double, lon: Double, exclude: String, appId: String, units: String): Response<Forecast> {
        return weatherDataFactory.getWeatherForecast(lat, lon, exclude, appId, units)
    }

    override fun getLastStoredLocationList(): List<Location> {
        return weatherDataFactory.getListOfLocations()
    }

    override fun addToLocationList(location: Location) {
        return weatherDataFactory.saveListOfLocations(location)
    }

    override fun deleteLocations(deleteLocationList:List<Location>) {
        return weatherDataFactory.deleteLocations(deleteLocationList)
    }
}