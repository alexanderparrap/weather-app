package com.forecast.climate

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ClimateForecastApp : Application() {

}