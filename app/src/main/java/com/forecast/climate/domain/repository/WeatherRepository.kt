package com.forecast.climate.domain.repository

import com.forecast.climate.domain.models.Forecast
import com.forecast.climate.domain.models.Location
import retrofit2.Response

interface WeatherRepository {
    suspend fun getForecast(lat: Double, lon: Double, exclude: String, appId: String, units: String): Response<Forecast>
    fun getLastStoredLocationList():List<Location>
    fun addToLocationList(location: Location)
    fun deleteLocations(deleteLocationList:List<Location>)
}