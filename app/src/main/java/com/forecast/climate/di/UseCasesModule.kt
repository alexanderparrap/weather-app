package com.forecast.climate.di

import com.forecast.climate.domain.repository.WeatherRepository
import com.forecast.climate.domain.usecases.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object UseCasesModule {

    @Singleton
    @Provides
    fun provideLocationUseCase(weatherRepository: WeatherRepository): LocationUseCase {
        return LocationUseCaseImpl(weatherRepository)
    }

    @Singleton
    @Provides
    fun provideWeatherForecastUseCase(weatherRepository: WeatherRepository): WeatherForecastUseCase {
        return WeatherForecastUseCaseImpl(weatherRepository)
    }

    @Singleton
    @Provides
    fun provideDateTransformationsUseCase(): DateTransformationUseCase {
        return DateTransformationUseCaseImpl()
    }
}