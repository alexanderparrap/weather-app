package com.forecast.climate.di

import android.content.Context
import android.content.SharedPreferences
import com.forecast.climate.data.WeatherDataRepository
import com.forecast.climate.data.factory.WeatherDataFactory
import com.forecast.climate.data.source.local.LocalDataSource
import com.forecast.climate.data.source.local.LocalDataSourceImpl
import com.forecast.climate.data.source.net.NetDataSource
import com.forecast.climate.data.source.net.NetDataSourceImpl
import com.forecast.climate.data.source.net.WeatherRestApi
import com.forecast.climate.data.util.Constants
import com.forecast.climate.domain.repository.WeatherRepository
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ApplicationModule {

    @Singleton
    @Provides
    fun provideDataFactory(netDataSource: NetDataSource, localDataSource: LocalDataSource): WeatherDataFactory{
        return WeatherDataFactory(netDataSource, localDataSource)
    }

    @Singleton
    @Provides
    fun provideNetDataSource(weatherRestApi: WeatherRestApi): NetDataSource{
        return NetDataSourceImpl(weatherRestApi)
    }

    @Singleton
    @Provides
    fun provideLocalDataSource(sharedPrefs: SharedPreferences, moshi: Moshi): LocalDataSource{
        return LocalDataSourceImpl(sharedPrefs, moshi)
    }

    @Singleton
    @Provides
    fun provideWeatherDataRepository(weatherDataFactory: WeatherDataFactory): WeatherRepository{
        return WeatherDataRepository(weatherDataFactory)
    }

    @Singleton
    @Provides
    fun provideSharedPreferences(@ApplicationContext context: Context): SharedPreferences {
        return context.getSharedPreferences(Constants.SHARED_PREFS, Context.MODE_PRIVATE)
    }

    @Singleton
    @Provides
    fun provideMoshiClient(): Moshi{
        return Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build()
    }
}